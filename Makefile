# Nom du fichier principal (sans .java)
MAIN = net/krafting/backtobeta/LauncherFrame

# Compilateur Java
JAVAC = javac

# Interpréteur Java
JAVA = java

# Options de compilation
JFLAGS = -g

# Répertoire de sortie des classes compilées
OUTDIR = bin

# Répertoire source
SRC_DIR = src

# Définition du classpath avec les bibliothèques externes
CP = jars/swinger-1.0.0-BETA.jar:jars/gson-2.3.1.jar:jars/openauth-1.0.3-SNAPSHOT.jar:jars/s-update-3.1.0-BETA.jar:jars/openlauncherlib-3.0.5.jar

# Nom du fichier JAR final
JAR_NAME = Launcher.jar

# Trouver tous les fichiers .java du projet
SRC = $(wildcard $(SRC_DIR)/net/krafting/backtobeta/*.java)

# Convertir les fichiers .java en fichiers .class
CLASSES = $(SRC:.java=.class)

# Extraire les JARs dans le dossier bin
extract-jars:
	@echo "Extraction des JARs..."
	@for jar in jars/*.jar; do \
		mkdir -p $(OUTDIR); \
		unzip -n $$jar -d $(OUTDIR); \
	done

# Compilation des fichiers .java
compile: extract-jars
	@echo "Compilation des fichiers .java..."
	@mkdir -p $(OUTDIR)
	$(JAVAC) -cp "$(CP)" $(JFLAGS) -d $(OUTDIR) $(SRC)

# Copier les ressources dans le dossier bin
copy-resources:
	@echo "Copie des ressources..."
	@mkdir -p $(OUTDIR)/net/krafting/backtobeta/ressources
	@cp -r $(SRC_DIR)/net/krafting/backtobeta/ressources/* $(OUTDIR)/net/krafting/backtobeta/ressources/

# Générer le fichier JAR
create-jar: compile copy-resources extract-jars
	@echo "Création du fichier JAR..."
	@jar cfe $(JAR_NAME) $(MAIN) -C $(OUTDIR) .

# Exécuter le programme
run: create-jar
	@echo "Exécution du programme..."
	$(JAVA) -jar $(JAR_NAME)

# Nettoyer les fichiers générés
clean:
	@echo "Nettoyage des fichiers compilés..."
	@rm -rf $(OUTDIR) $(JAR_NAME)

# Commande par défaut
all: create-jar

.PHONY: compile copy-resources create-jar run clean all
