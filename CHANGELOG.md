# Latest (Not released)

## Added

## Changed

## Removed

# Version 1.9.2

## Added
+ New Logo, match all versions

## Changed
* Fixed the bottom left corner not being transparent

## Removed

# Version 1.9.0

## Added

## Changed

* Replace user.home with the actual path of the launcher, fix issues on Windows
* The RAM slider now adapts even more to user's system
* Fixed mods not showing up in the settings on Windows

## Removed


# Version 1.8.7

## Added

+ Matrix instead of discord!
+ The RAM slider now adapts the amount of ram depending of what the system has
+ Rounded corners!
+ New EXE for Windows!

## Changed

* Changed font of buttons
* Changed height of the mod list, to allow small screen to work nicely

## Removed
