package net.krafting.backtobeta;

import javax.swing.JFrame;

import fr.theshark34.swinger.Swinger;
import fr.theshark34.swinger.util.WindowMover;
@SuppressWarnings("serial")
public class LauncherFrame extends JFrame {
	private static LauncherFrame instance;
	private LauncherPanel launcherPanel;
	private static CrashReporter crashReporter;
	public LauncherFrame() {
		this.setTitle("Back To Beta - Launcher");
		this.setSize(728, 508);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setUndecorated(true);
		this.setIconImage(Swinger.getResource("btb_icon_launcher.png"));
		this.setContentPane(launcherPanel = new LauncherPanel());
		WindowMover mover = new WindowMover(this);
		this.addMouseListener(mover);
		this.addMouseMotionListener(mover);
		this.setVisible(true);
	}
	public static void main(String[] args) {
		Swinger.setSystemLookNFeel();
		Swinger.setResourcePath("/net/krafting/backtobeta/ressources/");
		Launcher.BTB_DIR.mkdirs();
		crashReporter = new CrashReporter("Back To Beta - Launcher", Launcher.BTB_CRASHES_DIR);
		instance = new LauncherFrame();
	}
	public static LauncherFrame getInstance() {
		return instance;
	}
	public static CrashReporter getCrashReporter() {
			return crashReporter;
	} 
	public LauncherPanel getLauncherPanel() {
		return this.launcherPanel;
	}
}
