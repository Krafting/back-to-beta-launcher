package net.krafting.backtobeta;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextField;
import fr.theshark34.openauth.AuthenticationException;
import fr.theshark34.openlauncherlib.LaunchException;
import fr.theshark34.openlauncherlib.util.Saver;
import fr.theshark34.swinger.Swinger;
import fr.theshark34.swinger.colored.SColoredBar;
import fr.theshark34.swinger.event.SwingerEvent;
import fr.theshark34.swinger.event.SwingerEventListener;
import fr.theshark34.swinger.textured.STexturedButton;

@SuppressWarnings("serial")
public class LauncherPanel extends JPanel implements SwingerEventListener {
	public String version = "1.9.2";
	private Image background = Swinger.getResource("bg7.png");
	private Saver saver = new Saver(new File(Launcher.BTB_DIR, "launcher.properties"));
	private JTextField usernameField = new JTextField(this.saver.get("username"));
	private STexturedButton playButton = new STexturedButton(Swinger.getResource("jouer.png"));
	private STexturedButton quitButton = new STexturedButton(Swinger.getResource("quit.png"));
	private STexturedButton hideButton = new STexturedButton(Swinger.getResource("reduce.png"));
	private STexturedButton settingsButton = new STexturedButton(Swinger.getResource("reduce.png"));
	private STexturedButton webButton = new STexturedButton(Swinger.getResource("web.png"));
	private STexturedButton forumButton = new STexturedButton(Swinger.getResource("forum.png"));
	private STexturedButton chatButton = new STexturedButton(Swinger.getResource("matrix.png"));
	private SColoredBar progressBar = new SColoredBar(Color.decode("#ffffff"), Color.decode("#76e666"));
	private JLabel infoLabel = new JLabel("");
	
	// private RamSelector ramSelector = new RamSelector(new File(Launcher.BTB_DIR, "ram.txt"));
	public LauncherPanel() {
		this.setLayout(null);
	    final String varSystem = System.getProperty("os.name");
		System.out.println("The user is using: " + varSystem);
//		if(System.getProperty("os.name").startsWith("Windows")) {
//			usernameField.setForeground(Color.decode("#f1f1f1"));
//		}
		usernameField.setForeground(Color.decode("#f1f1f1"));
		usernameField.setBackground(Color.decode("#171b21"));
		usernameField.setFont(usernameField.getFont().deriveFont(40F));
		usernameField.setCaretColor(Color.decode("#f1f1f1"));
		usernameField.setOpaque(true);
		usernameField.setBorder(null);
		usernameField.setBounds(60, 394, 380, 50);
		this.add(usernameField);

		settingsButton.setBounds(625,4);
		settingsButton.addEventListener(this);
		this.add(settingsButton);
		
		playButton.setBounds(476,363);
		playButton.addEventListener(this);
		this.add(playButton);
		
		quitButton.setBounds(695,4);
		quitButton.addEventListener(this);
		this.add(quitButton);

		hideButton.setBounds(660,4);
		hideButton.addEventListener(this);
		this.add(hideButton);

		webButton.setBounds(24,271);
		webButton.addEventListener(this);
		this.add(webButton);

		forumButton.setBounds(258,271);
		forumButton.addEventListener(this);
		this.add(forumButton);
		
		chatButton.setBounds(493,271);
		chatButton.addEventListener(this);
		this.add(chatButton);
		
		progressBar.setBounds(24,331, 680, 20);
		this.add(progressBar);

		infoLabel.setForeground(Color.decode("#f1f1f1"));
		infoLabel.setFont(infoLabel.getFont().deriveFont(15F));
		infoLabel.setBounds(50,452, 680, 25);
		this.add(infoLabel);
	}
	@Override
	public void onEvent(SwingerEvent event) {
		if(event.getSource() == playButton) {
			setFieldEnable(false);
			if(usernameField.getText().replaceAll(" ", "").length() == 0) {
				JOptionPane.showMessageDialog(this, "Veuillez entrez un pseudo valide.", "Erreur", JOptionPane.ERROR_MESSAGE);
				setFieldEnable(true);
				return;
			}
			Thread t = new Thread() {
				@Override 
				public void run() {
					try {
						Launcher.auth(usernameField.getText(), "");
					} catch (AuthenticationException e) {
						LauncherFrame.getCrashReporter().catchError(e, "Impossible de se connecter :");
						setFieldEnable(true);
						return;
					}	
					
					System.out.println(Launcher.BTB_DIR);
					File f = new File(Launcher.BTB_DIR, "launcher.properties");
					if(!f.exists()) {
						try {
							saver.set("modded", "no"); 	
							saver.set("RAM", "2048"); 	
							Launcher.update();
						} catch (Exception e) {
							LauncherFrame.getCrashReporter().catchError(e, "Impossible de mettre à jour Back To Bêta :");
							System.out.print(e);
							Launcher.interruptThread();
							setFieldEnable(true);
							return;
						}
					}
					saver.set("username", usernameField.getText().replace(" ", ""));
					try {
						Launcher.launch();
					} catch (LaunchException e) {
						LauncherFrame.getCrashReporter().catchError(e, "Impossible de lancer le jeu :");
						setFieldEnable(true);
					}
				}
			};
			t.start();
		} else if(event.getSource() == quitButton) 
				System.exit(0);
		 else if(event.getSource() == hideButton) 
				LauncherFrame.getInstance().setState(JFrame.ICONIFIED);
		 else if(event.getSource() == webButton) 
	            try {
	                Desktop.getDesktop().browse(new URI("https://btb.krafting.net/"));
	            } catch (IOException e1) {
	                e1.printStackTrace();
	            } catch (URISyntaxException e1) {
	                e1.printStackTrace();
	            }
		 else if(event.getSource() == forumButton) 
	            try {
	                Desktop.getDesktop().browse(new URI("https://btb.krafting.net/forum.php"));
	            } catch (IOException e1) {
	                e1.printStackTrace();
	            } catch (URISyntaxException e1) {
	                e1.printStackTrace();
	            }
		else if(event.getSource() == chatButton)
				try {
					Desktop.getDesktop().browse(new URI("https://matrix.to/#/%23outbreaker:matrix.org"));
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}
		else if(event.getSource() == settingsButton) {
			setFieldEnable(false);
			Thread u = new Thread() {
				@Override 
				public void run() {
					JDialog.setDefaultLookAndFeelDecorated(true);
					String messageMod = "<html><body><h2 style='margin:0'>Voulez-vous utiliser la version moddée du Launcher Back To Beta ?</h2></body></html>";
					String messageMod2 = "Note: Changer l'état de cette case, forcera la mise à jour du jeu.";
					JCheckBox checkboxMod = new JCheckBox("Oui, utiliser la version moddée.");
					String moddedField = "";
					
					// On initie le fichier de config
					File f = new File(Launcher.BTB_DIR, "launcher.properties");
					if(!f.exists()) {
						saver.set("modded", "no");
						saver.set("RAM", "2048");
					}

					/* Le slider pour choisir la RAM */
					int currentSettingRam = Integer.parseInt(saver.get("RAM"));
					String messageMaxRam = "<html><body><h2 style='margin:0'>La RAM maximum que le jeu peu utiliser (en Mo)</h2></body></html>";
					
					long memorySize = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize();
					memorySize = Math.round(memorySize / (1024 * 1024));
					System.out.println(memorySize);
					int memorySizeChunk = (int) (memorySize / 8);
					
					JSlider maxRamAllocated = new JSlider(JSlider.HORIZONTAL, 1024, (int) memorySize, currentSettingRam);
					
					maxRamAllocated.setMajorTickSpacing(memorySizeChunk);
					maxRamAllocated.setMinorTickSpacing(1);
					maxRamAllocated.setPaintTicks(true);
					maxRamAllocated.setPaintLabels(true);

					
					/* Pour la liste des Mods */
					moddedField = saver.get("modded");
					if(moddedField.equals("yes")) {
						checkboxMod.setSelected(true);
					} else {
						checkboxMod.setSelected(false);
					}
					
					String messageSpace = "<html><br/></html>";
					
					String messageUpdate = "<html><body><h2 style='margin:0'>Voulez-vous mettre Back To Beta à jour ?</h2></body></html>";
					String messageUpdate2 = "Note: Ceci met uniquement les fichiers du jeu à jour.\nPour les mises à jour du launcher, veuillez vous rendre sur le site web.";
					JCheckBox checkboxUpdate = new JCheckBox("Oui, mettre à jour les fichiers du jeu.");
					String messageMods;
					List<String> modList = new ArrayList<>();
					List<String> enabledModList = new ArrayList<>();
					List<String> disabledModList = new ArrayList<>();

					File enabledMods = new File(Launcher.BTB_DIR, "mods/");
					File disabledMods = new File(Launcher.BTB_DIR, "mods/disabled");

					File[] enabledDirectoryListing = enabledMods.listFiles((d,fi)-> fi.toLowerCase().endsWith(".zip"));
					File[] disabledDirectoryListing = disabledMods.listFiles((d,fi)-> fi.toLowerCase().endsWith(".zip"));
					if (enabledDirectoryListing != null) {
					    for (File child : enabledDirectoryListing) {
					    	String value = child.getName();
					    	enabledModList.add(value);
					    	modList.add(value);
					    }
					}
					if (disabledDirectoryListing != null) {
					    for (File child : disabledDirectoryListing) {
					    	String value = child.getName();
					    	modList.add(value);
					    	disabledModList.add(value);
					    }
					}
					JPanel layoutPanel = new JPanel(new GridLayout(modList.size() - modList.size() / 2, 2));
					
					JCheckBox[] checkBoxListEnabled = new JCheckBox[enabledModList.size()];
					JCheckBox[] checkBoxListDisabled = new JCheckBox[disabledModList.size()];

					for(int i = 0; i < checkBoxListEnabled.length; i++) {
						checkBoxListEnabled[i] = new JCheckBox(enabledModList.get(i));
						checkBoxListEnabled[i].setSelected(true);
						layoutPanel.add(checkBoxListEnabled[i]);
					}
					for(int i = 0; i < checkBoxListDisabled.length; i++) {
						checkBoxListDisabled[i] = new JCheckBox(disabledModList.get(i));
						layoutPanel.add(checkBoxListDisabled[i]);
					}
						
					JScrollPane scroller = new JScrollPane(layoutPanel);
					if(moddedField.equals("yes")) {
						scroller.setPreferredSize(new Dimension(130, 125));
						messageMods = "<html><body><h2 style='margin:0'>Liste des mods activés:</h2></body></html>";
					} else {
						scroller.setPreferredSize(new Dimension(0, 0));
						messageMods = "";
					}

					String versionLauncher = "Version du launcher: " + version;
					
					Object[] params = {messageMod, messageMod2, checkboxMod, messageSpace, messageUpdate, messageUpdate2, checkboxUpdate, messageSpace, messageMaxRam, maxRamAllocated, messageSpace,messageMods, scroller, messageSpace, versionLauncher};
					JFrame frameOption = new JFrame();
					frameOption.setPreferredSize(new Dimension(500, 500));
					frameOption.setAlwaysOnTop(true);
				    frameOption.setResizable(true);
				    
				    // On affiche tout ca
				    int response = JOptionPane.showConfirmDialog(frameOption, params, "Paramètres", JOptionPane.OK_CANCEL_OPTION);
				    if (response == JOptionPane.OK_OPTION) {
				    	
				    	/* On sauve la RAM selectionné */
				    	saver.set("RAM", String.valueOf(maxRamAllocated.getValue()));
				    	
				    	/* Quand on active ou désactive des mods */
						for(JCheckBox box : checkBoxListEnabled) {
				            if(!box.isSelected())  { 
				                System.out.println("Disabling: " + box.getText());
				                try {
									Files.move(Paths.get(Launcher.BTB_DIR + "/mods/" + box.getText()), Paths.get(Launcher.BTB_DIR + "/mods/disabled/" + box.getText()));
								} catch (IOException e) {
									e.printStackTrace();
								}
				            }
				        }
				        for(JCheckBox box : checkBoxListDisabled) {
				            if(box.isSelected()) {
				                System.out.println("Enabling: " + box.getText());
				                try {
									Files.move(Paths.get(Launcher.BTB_DIR + "/mods/disabled/" + box.getText()), Paths.get(Launcher.BTB_DIR + "/mods/" + box.getText()));
								} catch (IOException e) {
									e.printStackTrace();
								}
				            }
				        }
				        
				        /* Si on change l'état du bouton Modded, alors on update en meme temps */
				    	if(checkboxMod.isSelected()) {
				    		if(saver.get("modded").equals("no")) {
								try {
									saver.set("modded", "yes");
									setFieldEnable(false);
									Launcher.update();
									setFieldEnable(true);
								} catch (Exception e) {
									Launcher.interruptThread();
									LauncherFrame.getCrashReporter().catchError(e, "Impossible de mettre à jour Back To Bêta :");
									setFieldEnable(true);
									return;
								}
				    		}
						} else {
				    		if(saver.get("modded").equals("yes")) {
								try {
									saver.set("modded", "no");
									setFieldEnable(false);
									Launcher.update();
									setFieldEnable(true);
								} catch (Exception e) {
									Launcher.interruptThread();
									LauncherFrame.getCrashReporter().catchError(e, "Impossible de mettre à jour Back To Bêta :");
									setFieldEnable(true);
									return;
								}
				    		}
						}
				    	
				    	/* Si on veux forcer les mises a jours manuellement */
				    	if(checkboxUpdate.isSelected()) {
							try {
								setFieldEnable(false);
								Launcher.update();
								setFieldEnable(true);
							} catch (Exception e) {
								Launcher.interruptThread();
								LauncherFrame.getCrashReporter().catchError(e, "Impossible de mettre à jour Back To Bêta :");
								setFieldEnable(true);
								return;
							}
				    	}
				    	setFieldEnable(true);
				    } else { 
				    	setFieldEnable(true);
				    }
			    	setFieldEnable(true);
				}
			};
			u.start();
	    	setFieldEnable(true);
		}
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), this);
	}
	public void setFieldEnable(boolean enabled) {
		usernameField.setEnabled(enabled);
		playButton.setEnabled(enabled);
		settingsButton.setEnabled(enabled);
	}
	public SColoredBar getProgressBar() {
		return progressBar;
	}
	public void setInfoText(String text) {
		infoLabel.setText(text);
	}
}
