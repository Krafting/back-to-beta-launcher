package net.krafting.backtobeta;

public final class SystemUtils {
	public final static String OS_NAME = System.getProperty("os.name");
	public final static String OS_VERSION = System.getProperty("os.version");
 
	public final static boolean is_OS_LINUX = "Linux".equals(OS_NAME);
	public final static boolean is_OS_MAC = "Mac".equals(OS_NAME);
	public final static boolean is_OS_MAC_OSX = "Mac OS X".equals(OS_NAME); 
}