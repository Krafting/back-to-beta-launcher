package net.krafting.backtobeta;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.theshark34.openauth.AuthenticationException;
import fr.theshark34.openlauncherlib.LaunchException;
import fr.theshark34.openlauncherlib.external.ExternalLaunchProfile;
import fr.theshark34.openlauncherlib.external.ExternalLauncher;
import fr.theshark34.openlauncherlib.minecraft.AuthInfos;
import fr.theshark34.openlauncherlib.minecraft.GameFolder;
import fr.theshark34.openlauncherlib.minecraft.GameInfos;
import fr.theshark34.openlauncherlib.minecraft.GameType;
import fr.theshark34.openlauncherlib.minecraft.GameVersion;
import fr.theshark34.openlauncherlib.minecraft.MinecraftLauncher;
import fr.theshark34.openlauncherlib.util.ProcessLogManager;
import fr.theshark34.openlauncherlib.util.Saver;
import fr.theshark34.supdate.BarAPI;
import fr.theshark34.supdate.SUpdate;
import fr.theshark34.supdate.application.integrated.FileDeleter;

public class Launcher {
	public static final GameVersion BTB_VERSION = new GameVersion("b1.7.3", GameType.V1_5_2_LOWER);
	public static final GameInfos BTB_INFOS = new GameInfos("backtobeta", BTB_VERSION, null);
	public static final File BTB_DIR = BTB_INFOS.getGameDir();
	public static Saver saver2 = new Saver(new File(Launcher.BTB_DIR, "launcher.properties"));
	public static final File BTB_CRASHES_DIR = new File(BTB_DIR, "crashes_logs");
	private static AuthInfos authInfos; 
	public static Thread updateThread;
	public static CrashReporter crashReporter = new CrashReporter("Crashes Dir", BTB_CRASHES_DIR);
	public static void auth(String username, String password) throws AuthenticationException {
		 //Authenticator authenticator = new Authenticator(Authenticator.MOJANG_AUTH_URL, AuthPoints.NORMAL_AUTH_POINTS);
		 //AuthResponse response = authenticator.authenticate(AuthAgent.MINECRAFT, username, password, "");
		 //authInfos = new AuthInfos(response.getSelectedProfile().getName(), response.getAccessToken(), response.getSelectedProfile().getId());
		 authInfos = new AuthInfos(username, "sry", "nope");
	}
	public static void update()  {
		saver2.load();
		String moddedField = saver2.get("modded");
		String urlDownload;
		if(moddedField.equals("yes")) {
			urlDownload = "https://www.krafting.net/0/backtobeta/dl/update/modded/";
		} else {
			urlDownload = "https://www.krafting.net/0/backtobeta/dl/update/vanilla/";
		}
		SUpdate su = new SUpdate(urlDownload, BTB_DIR);
		su.addApplication(new FileDeleter());
		Thread updateThread = new Thread() {
			private int val;
			private int max;
			@Override
			public void run() {
				while(!this.isInterrupted()) {
					if(BarAPI.getNumberOfTotalBytesToDownload() == 0) {
						LauncherFrame.getInstance().getLauncherPanel().setInfoText("Vérification des fichiers..");
						continue;
					}
					val = (int) (BarAPI.getNumberOfDownloadedFiles());
					max = (int) (BarAPI.getNumberOfFileToDownload());
					LauncherFrame.getInstance().getLauncherPanel().getProgressBar().setMaximum(max);
					LauncherFrame.getInstance().getLauncherPanel().getProgressBar().setValue(val);
					LauncherFrame.getInstance().getLauncherPanel().setInfoText("Téléchargement des fichiers " +
						BarAPI.getNumberOfDownloadedFiles() + " / " + BarAPI.getNumberOfFileToDownload() + " - " + 
							val * 100 / max  + "%"); 
				}
				LauncherFrame.getInstance().getLauncherPanel().setInfoText("Back To Bêta est à jour!");
			}
		};
		updateThread.start();
		try {
			su.start();
		} catch(final Exception e) {
			Thread tss = new Thread() {
				@Override 
				public void run() {
					LauncherFrame.getCrashReporter().catchError(e, "Impossible de mettre à jour Back To Bêta :");
				}
			};
			tss.start();
			System.out.println(e);
		}
		updateThread.interrupt();
	}
	public static void launch() throws LaunchException {
		ExternalLaunchProfile profile = MinecraftLauncher.createExternalProfile(BTB_INFOS, GameFolder.BASIC, authInfos);

		// Ajouter des arguments à la VM Java
	    List<String> currentArgs = profile.getVmArgs();
	    List<String> ramSelector = new ArrayList<String>();
	    List<String> newArgs = new ArrayList<String>();

	    // Ajoute les arguments de la RAM
		saver2.load();
		int maxRam = Integer.parseInt(saver2.get("RAM"));
		int minRam = maxRam - 512;
	    ramSelector.add("-Xms" + minRam + "M");
	    ramSelector.add("-Xmx" + maxRam + "M");
	    newArgs.addAll(ramSelector);
	    newArgs.addAll(currentArgs);
	    
	    // On crée le tout
		profile.setVmArgs(newArgs);
		ExternalLauncher launcher = new ExternalLauncher(profile);
		Process p = launcher.launch();
		ProcessLogManager manager = new ProcessLogManager(p.getInputStream(), new File(BTB_DIR, "logs.txt"));
		manager.start();
		
		// On quitte le launcher
		System.exit(1);
	}
	public static void interruptThread() {
		//if(updateThread != null) updateThread.interrupt();
		updateThread.interrupt();
	}
}
