package net.krafting.backtobeta;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

public class CrashReporter { 
    private File dir;
    private String name;
    public CrashReporter(String name, File dir) {
        this.name = name;
        this.dir = dir;
    }

    public void catchError(Exception e, String message) {
        System.out.println(makeCrashReport(name, e));
        String msg;
        try {
            File report = writeError(e);
            msg = "\nThe crash report is in : \n" + report.getAbsolutePath() + "";
        }
        catch (IOException e2) {
            e.printStackTrace();
            msg = "\nAnd unable to write the crash report : \n" + e2;
        }
        JOptionPane.showMessageDialog(null, message + "\n" + e + "\n" + msg, "Erreur", JOptionPane.ERROR_MESSAGE);
    }

    public File writeError(Exception e) throws IOException {
    	File crash_dir = new File(dir.toString());
    	if(!crash_dir.exists()) {
    		try {
    			crash_dir.mkdir();
    		} catch (SecurityException se) {
    			// Handle it later
    		}
    	}
        File file;
        DateFormat dateFormats = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
        Date dates = new Date();
        while ((file = new File(dir, "crash-" + dateFormats.format(dates) + "-btb.txt")).exists())
	        file.getParentFile().mkdirs();
        	FileWriter f = new FileWriter(file);
	        f.write(makeCrashReport(name, e));
	        f.close();
	        return file;
    } 

    public static String makeCrashReport(String projectName, Exception e) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        StringBuilder report = new StringBuilder()
                .append("# ").append(projectName).append(" Crash Report\n")
                .append("#\n# Date : ").append(dateFormat.format(date)).append("")
                .append("\n# Exception : ").append(e.getClass().getSimpleName()).append("\n\n")
                .append("\n# ").append(e.toString());

        StackTraceElement[] stackTrace = e.getStackTrace();
        for (StackTraceElement element : stackTrace)
            report.append("\n#     ").append(element);

        Throwable cause = e.getCause();
        if (cause != null) {
            report.append("\n\r# Causé par: ").append(cause);

            StackTraceElement[] causeStackTrace = cause.getStackTrace();
            for (StackTraceElement element : causeStackTrace)
                report.append("\n\r#     ").append(element);
        }
        return report.toString();
    }
}
