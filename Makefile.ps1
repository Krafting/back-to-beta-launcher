# Nom du fichier principal (sans .java)
$MAIN = "net.krafting.backtobeta.LauncherFrame"

# Compilateur Java
$JAVAC = "javac"

# Interpréteur Java
$JAVA = "java"

# Options de compilation
$JFLAGS = "-g"

# Répertoire de sortie des classes compilées
$OUTDIR = "bin"

# Répertoire source
$SRC_DIR = "src"

# Définition du classpath avec les bibliothèques externes
$CP = "jars/swinger-1.0.0-BETA.jar;jars/gson-2.3.1.jar;jars/openauth-1.0.3-SNAPSHOT.jar;jars/s-update-3.1.0-BETA.jar;jars/openlauncherlib-3.0.5.jar"

# Trouver tous les fichiers .java du projet
$SRC = Get-ChildItem -Recurse -Filter "*.java" -Path "$SRC_DIR\net\krafting\backtobeta"

# Règle pour créer le fichier JAR
$JAR_NAME = "Launcher.jar"

# Règle par défaut (compilation)
Function Extract-Jars {
    # Extraire tous les fichiers JAR dans le dossier bin/
    $jarFiles = Get-ChildItem -Path "jars" -Filter "*.jar"
    foreach ($jar in $jarFiles) {
        # Extraire le contenu du JAR dans le dossier temporaire
        Expand-Archive -Path $jar.FullName -DestinationPath $OUTDIR -Force
    }
}

Function Compile {
    # Créer le dossier de sortie
    if (-Not (Test-Path -Path $OUTDIR)) {
        New-Item -ItemType Directory -Force -Path $OUTDIR
    }
    
    # Compilation des fichiers .java
    & $JAVAC -cp "jars/*" $JFLAGS -d $OUTDIR $SRC.FullName
}

# Copier les ressources (images) dans le dossier bin/
Function Copy-Resources {
    $resourceDir = "$SRC_DIR\net\krafting\backtobeta\ressources"
    $outResourceDir = "$OUTDIR\net\krafting\backtobeta\ressources"

    if (-Not (Test-Path -Path $outResourceDir)) {
        New-Item -ItemType Directory -Force -Path $outResourceDir
    }

    Copy-Item -Recurse -Path $resourceDir\* -Destination $outResourceDir
}

# Exécuter le programme avec les dépendances
Function Run {
    & $JAVA -cp "$OUTDIR;$CP" $MAIN
}

# Règle pour générer le JAR
Function Create-Jar {
    Compile
    Copy-Resources
    Extract-Jars
    Run
    
    # Créer le fichier JAR
    & jar cfe $JAR_NAME $MAIN -C $OUTDIR .
}

# Nettoyer les fichiers compilés
Function Clean {
    Remove-Item -Recurse -Force $OUTDIR
    Remove-Item -Force $JAR_NAME
}

# Choix de la commande à exécuter
$command = Read-Host "Entrez la commande à exécuter (compile, run, jar, clean)"
switch ($command) {
    "compile" { Compile }
    "run" { Run }
    "jar" { Create-Jar }
    "clean" { Clean }
    default { Write-Host "Commande inconnue" }
}
