# Back To Beta Launcher

My own Back To Beta Launcher for my minecraft server. This launcher is configured to connect to a SUpdate server.

# Features :

* Version b1.7.3 of Minecraft
* Optifine by default
* A Mod to fix the skins of players by default
* Option to download a Modded version, with compatible mods selected by me
* Option to disable mods to your liking
* Link to the website, forum and matrix server
* Fully Open-Source !

# Future Updates :

* Locales
* Option to put the window controls to the left!
* Any ideas ? Create an issue here, on Gitlab

# Source Code Infos :

* You need the jars files in the jars/ folder in your Build Path on your IDE
* This is made using java 1.8.0_262
* Any infos you need ? Create an issue !

# Build this project

```shell
mkdir bin
/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.442.b06-1.fc41.x86_64/bin/javac -cp "jars/*" src/net/krafting/backtobeta/*.java
cp src/net/krafting/backtobeta/ressources/ bin/net/krafting/backtobeta/ -r
ant
```

## Execute the resulting jar file

```shell
java -jar Launcher.jar
```

# Build Other Way

```shell
mkdir bin
# Compile Java classes
/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.442.b06-1.fc41.x86_64/bin/javac -cp "jars/*" src/net/krafting/backtobeta/*.java -d bin
# Copy ressources files
cp src/net/krafting/backtobeta/ressources/ bin/net/krafting/backtobeta/ -r
# Extract dependencies JAR files
unzip -n jars/swinger-1.0.0-BETA.jar -d bin/
unzip -n jars/gson-2.3.1.jar -d bin/
unzip -n jars/openauth-1.0.3-SNAPSHOT.jar -d bin/
unzip -n jars/openlauncherlib-3.0.5.jar -d bin/
unzip -n jars/s-update-3.1.0-BETA.jar -d bin/
# Compile the Jar file
/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.442.b06-1.fc41.x86_64/bin/jar cvfe Launcher.jar net.krafting.backtobeta.LauncherFrame -C bin . 
```

# Clean UP

```shell
rm -rf bin
rm Launcher.jar
```